// A vampire number has an even number of digits and is formed by multiplying a pair of numbers containing half the number of digits of the result.
// The digits are taken from the original number in any order. Pairs of trailing zeroes are not allowed.
// This program finds all the 4-digit vampire numbers. The program does not produce duplicates, and is optimized by using Pete Hartley’s
// theoretical result (see the comment inside main( )).

package com.epam.task1;

public class VampireNumbers {
    public static void main(String[] args) {
        int[] startDigits = new int[4];
        int[] resultDigits = new int[4];
        for (int factor1 = 10; factor1 <= 99; factor1++)
            for (int factor2 = factor1; factor2 <= 99; factor2++) {
                // Pete Hartley's theoretical result: if x * y is a vampire number then x * y == x + y (mod 9)
                if ((factor1 * factor2) % 9 != (factor1 + factor2) % 9)
                    continue;
                int product = factor1 * factor2;
                startDigits[0] = factor1 / 10;
                startDigits[1] = factor1 % 10;
                startDigits[2] = factor2 / 10;
                startDigits[3] = factor2 % 10;
                resultDigits[0] = product / 1000;
                resultDigits[1] = (product % 1000) / 100;
                resultDigits[2] = product % 1000 % 100 / 10;
                resultDigits[3] = product % 1000 % 100 % 10;
                int count = 0;
                for (int x = 0; x < 4; x++)
                    for (int y = 0; y < 4; y++) {
                        if (resultDigits[x] == startDigits[y]) {
                            count++;
                            resultDigits[x] = -1;
                            startDigits[y] = -2;
                            if (count == 4)
                                System.out.println(factor1 + " * " + factor2 + " = " + product);
                        }
                    }
            }
    }
}